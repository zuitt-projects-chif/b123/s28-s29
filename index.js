const express = require("express");

const app = express();

const port = 4000;

app.use(express.json());

const users = [
  {
    email: "mighty12@gmail.com",
    username: "mightyMouse12",
    password: "notrelatedtomickey",
    isAdmin: false,
  },
  {
    email: "minnieMouse@gmail.com",
    username: "minniexmickey",
    password: "minniesincethestart",
    isAdmin: false,
  },
  {
    email: "mickeyTheMouse@gmail.com",
    username: "mickeyKing",
    password: "thefacethatrunstheplace",
    isAdmin: true,
  },
];
const items = [
  // {
  //   name: "Apple",
  //   description: "fresh fruit",
  //   price: 300,
  //   isActive: true,
  // },
  // {
  //   name: "Banana",
  //   description: "fresh fruit",
  //   price: 250,
  //   isActive: true,
  // },
];

let loggedUser;

// [GET] /
app.get("/", (req, res) => {
  res.json("Hello World");
});

// [POST] /hello
app.post("/hello", (req, res) => {
  console.log(req.body);
  res.json("Hello from batch 123 ");
});
// [POST] /
app.post("/", (req, res) => {
  const { name, age, description } = req.body;
  res.send(
    `Hello, Im ${name}. I am ${age}. I could be described as ${description}`
  );
});

// register
app.post("/users", (req, res) => {
  console.log("body", req.body);
  const { email, username, isAdmin, password } = req.body;
  const newUser = {
    email,
    username,
    password,
    isAdmin,
  };
  users.push(newUser);
  res.send("Registered succesfully");
});

// login
app.post("/users/login", (req, res) => {
  console.log("body", req.body);
  const { username, password } = req.body;
  const foundUserIndex = users.findIndex((user) => {
    return user.username === username && user.password === password;
  });
  if (foundUserIndex !== -1) {
    loggedUser = users[foundUserIndex];
    loggedUser.index = foundUserIndex;
    console.log(loggedUser);
    res.send("Thank you for logging in");
  } else {
    res.send("Login failed. Wrong Credentials");
  }
});

// addItem
app.post("/items", (req, res) => {
  console.log("loggedUser", loggedUser);
  console.log("body", req.body);
  const { name, description, price, isActive } = req.body;
  if (loggedUser && loggedUser.isAdmin) {
    const newItem = { name, description, price, isActive };
    items.push(newItem);
    console.log("items", items);
    res.status(201).send("You have added a new item");
  } else {
    res.status(403).send("Unauthorized: Action Forbidden");
  }
});

// getAllItem
app.get("/items", (req, res) => {
  console.log("loggedUser", loggedUser);
  console.log("body", req.body);
  if (loggedUser && loggedUser.isAdmin) {
    res.json(items);
  } else {
    res.status(403).send("Unauthorized: Action Forbidden");
  }
});

// getSingleUser
app.get("/users/:index", (req, res) => {
  const index = parseInt(req.params.index); //url is string, need to parse to number
  const user = users[index];
  res.send(user);
});

//updateUser
app.put("/users/:index", (req, res) => {
  if (loggedUser !== undefined && loggedUser.index == req.params.index) {
    const userIndex = parseInt(req.params.index); //url is string, need to parse to number
    users[userIndex].password = req.body.password;
    console.log("updated User", users[userIndex]);
    res.send("Updated password!");
  } else {
    res.status(401).send("Unauthorized. Login the correct user first");
  }
});

/// ACTIVITY 2

// get Single Item
app.get("/items/getSingle/:index", (req, res) => {
  const index = parseInt(req.params.index); //url is string, need to parse to number
  const item = items[index];
  if (item) {
    res.send(item);
  } else {
    res.status(404).send("Item not found");
  }
});

// archiveItem
app.put("/items/archive/:index", (req, res) => {
  if (loggedUser !== undefined && loggedUser.isAdmin) {
    const targetIndex = parseInt(req.params.index); //url is string, need to parse to number
    items[targetIndex].isActive = false;
    console.log("updated Item", items[targetIndex]);
    res.send(`Item ${items[targetIndex].name} Archived`);
  } else {
    res.status(403).send("Unauthorized. Action Forbidden");
  }
});

// activate
app.put("/items/activate/:index", (req, res) => {
  if (loggedUser !== undefined && loggedUser.isAdmin) {
    const targetIndex = parseInt(req.params.index); //url is string, need to parse to number
    items[targetIndex].isActive = true;
    console.log("updated Item", items[targetIndex]);
    res.send(`Item ${items[targetIndex].name} Activated`);
  } else {
    res.status(403).send("Unauthorized. Action Forbidden");
  }
});

app.listen(port, () => console.log(`Server is running at port ${port}`));
